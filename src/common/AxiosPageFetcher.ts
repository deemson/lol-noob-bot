import {AxiosStatic} from "axios";
import {Logger} from "winston";
import {PageFetcher} from "./PageFetcher";

export class AxiosPageFetcher implements PageFetcher {
    constructor(private logger: Logger, private axios: AxiosStatic) { }

    async fetch(url: string): Promise<string> {
        this.logger.info({
            message: "fetching page",
            url,
        });
        const response = await this.axios.get<string>(url);
        return response.data;
    }
}
