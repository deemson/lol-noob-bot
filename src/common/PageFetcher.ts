export interface PageFetcher {
    fetch(url: string): Promise<string>;
}
