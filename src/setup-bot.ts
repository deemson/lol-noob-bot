import axios from "axios";
import * as crypto from "crypto";
import * as _ from "lodash";
import Telegram, {Context, ContextMessageUpdate} from "telegraf";
import {Logger} from "winston";
import {AxiosPageFetcher} from "./common/AxiosPageFetcher";
import {PageFetcher} from "./common/PageFetcher";
import {Champion} from "./configs/champions/Champion";
import {RUNES_CONFIG} from "./configs/runes";
import {logger as rootLogger} from "./logger";
import {findChampions} from "./mobafire/find-champion";
import {GuidesFetcher} from "./mobafire/guides/GuidesFetcher";
import {GuidesTelegramRenderer} from "./mobafire/guides/GuidesTelegramRenderer";
import {BuildsFetcher} from "./mobafire/runes/BuildsFetcher";
import {BuildsTelegramRenderer} from "./mobafire/runes/BuildsTelegramRenderer";

const shortenChampions = (champions: Champion[]): string => {
    if (champions.length < 4) {
        return champions.map((champion) => champion.text).join(", ");
    } else {
        return champions.length.toString();
    }
};

interface LoLContext {
    logger: Logger;
    pageFetcher: PageFetcher;
    guidesFetcher: GuidesFetcher;
    guidesRenderer: GuidesTelegramRenderer;
    buildsFetcher: BuildsFetcher;
    buildsRenderer: BuildsTelegramRenderer;
}

export const setupBot = (token: string): Telegram<any> => {
    const bot = new Telegram(token);
    bot.use((context: any, next: (context: any) => any) => {
        const requestID = crypto.randomBytes(8).toString("hex");
        const updateType = context.updateType;
        const from = context.message.from.username;
        let chatInfo: Record<string, string> = {};
        if (!_.isUndefined(context.chat) && context.chat.type !== "private") {
            chatInfo = {chat: context.chat.title};
        }
        const logger = rootLogger.child({requestID});
        logger.info("got new request", {updateType, from, ...chatInfo});
        const pageFetcher = new AxiosPageFetcher(logger, axios);
        context.lol = {
            logger,
            pageFetcher,
            guidesFetcher: new GuidesFetcher(pageFetcher),
            guidesRenderer: new GuidesTelegramRenderer(),
            buildsFetcher: new BuildsFetcher(pageFetcher),
            buildsRenderer: new BuildsTelegramRenderer(RUNES_CONFIG),
        };
        return next(context);
    });
    bot.command("champions", async (context: any) => {
        const lolContext: LoLContext = context.lol;
        lolContext.logger.info("champions request");
        const query = context.message.text.slice("/champions ".length);
        const champions = findChampions(query);
        const title = `Searching champions by "_${query}_":`;
        let result = "";
        if (champions.length === 0) {
            result = "No champions were found";
        } else if (champions.length > 5) {
            result = `${champions.length} match query`;
        } else {
            result = champions.map((champion) => champion.text).join("\n");
        }
        return context.replyWithMarkdown(`${title}\n${result}`);
    });
    bot.command("guides", async (context: any) => {
        const lolContext: LoLContext = context.lol;
        lolContext.logger.info("guides request");
        const query = context.message.text.slice("/guides ".length);
        const champions = findChampions(query);
        await context.replyWithMarkdown(`Fetching champion guides for "_${query}_"...`);
        let result = "";
        if (champions.length === 0) {
            result = "No champions were found";
        } else if (champions.length > 1) {
            result = `Multiple (${shortenChampions(champions)}) champions were found: please narrow the query`;
        } else {
            const championID = champions[0].mobafireID;
            const guides = await lolContext.guidesFetcher.fetchGuides(championID);
            result = lolContext.guidesRenderer.render(guides);
        }
        return context.replyWithMarkdown(result, {
            disable_web_page_preview: true,
        });
    });
    bot.command("runes", async (context: any) => {
        const lolContext: LoLContext = context.lol;
        lolContext.logger.info("runes request");
        const match = context.message.text.match(/\/runes (\w+)(?: (\d+))?/);
        if (_.isNull(match) || match.length !== 3) {
            const part1 = `Request "${context.message.text}" does not match`;
            const part2 = "/runes <champion query (letters only)> [optional:<guide#(number)>]";
            return context.reply(`${part1}\n${part2}`);
        } else {
            const championQuery = match[1];
            const guideIndex = _.isUndefined(match[2]) ? 1 : parseInt(match[2], 10);
            const queryReply = `Fetching runes in guide#${guideIndex} for "_${championQuery}_"...`;
            await context.replyWithMarkdown(queryReply);
            const champions = findChampions(championQuery);
            if (champions.length === 0) {
                return context.replyWithMarkdown("No champions were found");
            } else if (champions.length > 1) {
                const r = `Multiple (${shortenChampions(champions)}) champions were found: please narrow the query`;
                return context.replyWithMarkdown(r);
            } else {
                const championID = champions[0].mobafireID;
                const guide = await lolContext.guidesFetcher.fetchGuideByIndex(championID, guideIndex - 1);
                const title = `Guide #${guideIndex} for ${champions[0].text}`;
                const descr = guide.description;
                const link = `[Link](${guide.url})`;
                const votes = guide.votes.toString();
                await context.replyWithMarkdown(`${title}\n${descr}\nVotes: ${votes}\n${link}`, {
                    disable_web_page_preview: true,
                });
                const builds = await lolContext.buildsFetcher.fetch(championID, guide);
                return context.replyWithMarkdown(lolContext.buildsRenderer.render(builds));
            }
        }
    });
    return bot;
};
