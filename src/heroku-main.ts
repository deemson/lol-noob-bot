import express from "express";
import {logger} from "./logger";
import {setupBot} from "./setup-bot";

const PORT = process.env.PORT || 3000;
const BOT_TOKEN = process.env.BOT_TOKEN;
const bot = setupBot(BOT_TOKEN);
const app = express();
app.get("/", (req, res) => {
    bot.telegram.setWebhook(`https://${req.hostname}/telegram-webhook`);
    res.send("webhook registered");
});
app.use(bot.webhookCallback("/telegram-webhook"));
app.listen(PORT, () => {
    logger.info(`LoL Noob bot listening`, {port: PORT});
});
