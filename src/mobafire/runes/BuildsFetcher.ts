import * as cheerio from "cheerio";
import {PageFetcher} from "../../common/PageFetcher";
import {Guide} from "../guides/Guide";
import {Build} from "./Build";
import {Primary} from "./Primary";
import {Secondary} from "./Secondary";
import {Shards} from "./Shards";
import {Variant} from "./Variant";

export class BuildsFetcher {
    constructor(private readonly pageFetcher: PageFetcher) { }

    async fetch(championID: string, guide: Guide): Promise<Build[]> {
        const html = await this.pageFetcher.fetch(guide.url);
        const $ = cheerio.load(html);
        const buildDescriptions = this.parseBuildDescriptions($);
        return this.parseBuilds(buildDescriptions, $);
    }

    private parseBuildDescriptions($: CheerioStatic): string[] {
        const descriptions: string[] = [];
        $(".view-guide__champs ul li span").each((idx: number, el: CheerioElement) => {
            descriptions.push(el.children[0].data);
        });
        if (descriptions.length === 0) {
            descriptions.push("");
        }
        return descriptions;
    }

    private parseBuilds(descriptions: string[], $: CheerioStatic): Build[] {
        const builds: Build[] = [];
        $(".view-guide__build .view-guide__build__runes").each((idx: number, el: CheerioElement) => {
            const variantDescriptions = this.parseVariantDescriptions($, el);
            const variants = this.parseVariants(variantDescriptions, $, el);
            builds.push(new Build(descriptions[idx], variants));
        });
        return builds;
    }

    private parseVariantDescriptions($: CheerioStatic, buildEl: CheerioElement): string[] {
        const descriptions: string[] = [];
        $(".view-guide__build__switcher span", buildEl).each((idx: number, el: CheerioElement) => {
            descriptions.push(el.attribs.title);
        });
        return descriptions;
    }

    private parseVariants(descriptions: string[], $: CheerioStatic, buildEl: CheerioElement): Variant[] {
        const variants: Variant[] = [];
        $(".new-runes", buildEl).each((idx: number, variantEl: CheerioElement) => {
            variants.push(this.parseVariant(descriptions[idx], $, variantEl));
        });
        return variants;
    }

    private parseVariant(description: string, $: CheerioStatic, variantEl: CheerioElement): Variant {
        const primary = this.parsePrimary($, variantEl);
        const secondary = this.parseSecondary($, variantEl);
        const shards = this.parseShards($, variantEl);
        return new Variant(description, primary, secondary, shards);
    }

    private parsePrimary($: CheerioStatic, variantEl: CheerioElement): Primary {
        const el = $(".new-runes__primary", variantEl).first();
        const path = el.attr().path;
        const slots: string[] = [];
        $(".new-runes__item span", el).slice(1).each((idx: number, slotEl: CheerioElement) => {
            slots.push(slotEl.children[0].data);
        });
        const [keystone, slot1, slot2, slot3] = slots;
        return new Primary(path, keystone, slot1, slot2, slot3);
    }

    private parseSecondary($: CheerioStatic, variantEl: CheerioElement): Secondary {
        const el = $(".new-runes__secondary", variantEl).first();
        const path = el.attr().path;
        const slots: string[] = [];
        $(".new-runes__item span", el).slice(1).each((idx: number, slotEl: CheerioElement) => {
            slots.push(slotEl.children[0].data);
        });
        const [slot1, slot2] = slots;
        return new Secondary(path, slot1, slot2);
    }

    private parseShards($: CheerioStatic, variantEl: CheerioElement): Shards | undefined {
        const slots: string[] = [];
        const selector = $(".new-runes__shards span", variantEl);
        if (selector.length === 0) {
            return undefined;
        }
        selector.each((idx: number, slotEl: CheerioElement) => {
            slots.push(slotEl.attribs["shard-type"]);
        });
        const [slot1, slot2, slot3] = slots;
        return new Shards(slot1, slot2, slot3);
    }
}
