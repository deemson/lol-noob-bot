import {Primary} from "./Primary";
import {Secondary} from "./Secondary";
import {Shards} from "./Shards";

export class Variant {
    constructor(readonly description: string = "",
                readonly primary: Primary,
                readonly secondary: Secondary,
                readonly shards: Shards) { }
}
