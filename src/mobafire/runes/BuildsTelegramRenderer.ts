import * as _ from "lodash";
import {Config} from "../../configs/runes/Config";
import {Build} from "./Build";
import {Primary} from "./Primary";
import {Secondary} from "./Secondary";
import {Shards} from "./Shards";
import {Variant} from "./Variant";

export class BuildsTelegramRenderer {
    constructor(readonly config: Config) { }

    render(builds: Build[]): string {
        return builds.map((build: Build, index: number) => this.renderBuild(build, index)).join("\n\n");
    }

    private renderBuild(build: Build, index: number): string {
        let title = `*Build #${index + 1}*`;
        if (build.description.length > 0) {
            title = `${title}: ${build.description}`;
        }
        const lines = [title, "------"];
        const variants = build.variants.map((variant: Variant, vIdx: number) => this.renderVariant(variant, vIdx));
        lines.push(variants.join("\n---\n"));
        return lines.join("\n");
    }

    private renderVariant(variant: Variant, index: number): string {
        let title = `Variant #${index + 1}`;
        if (variant.description.length > 0) {
            title = `${title}: ${variant.description}`;
        }
        return [
            title,
            this.renderPrimary(variant.primary),
            this.renderSecondary(variant.secondary),
            this.renderShards(variant.shards),
        ].join("\n");
    }

    private renderPrimary(primary: Primary): string {
        const path = this.config.pathByMobafireID(primary.path);
        const keystone = path.keystoneByMobafireID(primary.keystone);
        const slots = path.runesMobafirePrimary(primary.slot1, primary.slot2, primary.slot3);
        const slotsText = slots.map((s) => s.text).join(", ");
        return `_${path.text.toUpperCase()}_: ${keystone.text}, ${slotsText}`;
    }

    private renderSecondary(secondary: Secondary): string {
        const path = this.config.pathByMobafireID(secondary.path);
        const [slot1, slot2] = path.runesMobafireSecondary(secondary.slot1, secondary.slot2);
        return `_${path.text.toUpperCase()}_: ${slot1.text}, ${slot2.text}`;
    }

    private renderShards(shards: Shards): string {
        let shardsText = "-";
        if (!_.isUndefined(shards)) {
            const slots = this.config.shardsMobafire(shards.slot1, shards.slot2, shards.slot3);
            shardsText = slots.map((s) => s.text).join(", ");
        }
        return `_SHARDS_: ${shardsText}`;
    }
}
