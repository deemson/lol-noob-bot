import {Variant} from "./Variant";

export class Build {
    constructor(readonly description: string = "", readonly variants: Variant[]) { }
}
