export class Primary {
    constructor(readonly path: string,
                readonly keystone: string,
                readonly slot1: string,
                readonly slot2: string,
                readonly slot3: string) { }
}
