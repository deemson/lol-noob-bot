export class Secondary {
    constructor(readonly path: string, readonly slot1: string, readonly slot2: string) { }
}
