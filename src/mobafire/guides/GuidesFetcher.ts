import * as cheerio from "cheerio";
import * as _ from "lodash";
import {PageFetcher} from "../../common/PageFetcher";
import {MOBAFIRE_DOMAIN} from "./domain";
import {Guide} from "./Guide";

export const GUIDES_LIST_CLASS = "browse-list";
export const GUIDES_ITEM_CLASS = "browse-list__item";
export const GUIDE_TITLE_CLASS = "browse-list__item__desc__title";
export const GUIDE_RATING_CLASS = "browse-list__item__rating__total";

const parseGuide = (index: number, $: CheerioStatic, guideEl: CheerioElement): Guide => {
    const description = $(`.${GUIDE_TITLE_CLASS}`, guideEl).first().text();
    let votesText = $(`.${GUIDE_RATING_CLASS}`, guideEl)[0].children[1].data;
    votesText = votesText.replace(/,/g, "");
    const votes = parseInt(votesText, 10);
    const url = `${MOBAFIRE_DOMAIN}${guideEl.attribs.href}`;
    return new Guide(index, description, votes, url);
};

export class GuidesFetcher {
    constructor(private readonly pageFetcher: PageFetcher) { }

    async fetchGuides(championID: string, limit: number = 3): Promise<Guide[]> {
        const [$, $guides] = await this.guidesCheerio(championID);
        const guides: Guide[] = [];
        $guides.slice(0, limit).each((idx: number, guideEl: CheerioElement) => {
            guides.push(parseGuide(idx, $, guideEl));
        });
        return guides;
    }

    async fetchGuideByIndex(championID: string, index: number): Promise<Guide> {
        const [$, $guides] = await this.guidesCheerio(championID);
        let guide: Guide;
        $guides.each((idx: number, guideEl: CheerioElement) => {
            if (idx === index) {
                guide = parseGuide(index, $, guideEl);
                return;
            }
        });
        if (_.isUndefined(guide)) {
            throw new Error(`guide with index ${index} does not exist for "${championID}"`);
        } else {
            return guide;
        }
    }

    private async guidesCheerio(championID: string): Promise<[CheerioStatic, Cheerio]> {
        const url = `${MOBAFIRE_DOMAIN}/league-of-legends/${championID}-guide?sort=top&order=&author=all&page=1`;
        const html = await this.pageFetcher.fetch(url);
        const $ = cheerio.load(html);
        return [$, $(`.${GUIDES_LIST_CLASS} .${GUIDES_ITEM_CLASS}`)];
    }
}
