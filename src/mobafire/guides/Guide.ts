export class Guide {
    constructor(
        readonly index: number,
        readonly description: string = "",
        readonly votes: number,
        readonly url: string) { }
}
