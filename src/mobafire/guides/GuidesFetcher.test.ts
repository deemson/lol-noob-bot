import {PageFetcher} from "../../common/PageFetcher";
import {MOBAFIRE_DOMAIN} from "./domain";
import {Guide} from "./Guide";
import {
    GUIDE_RATING_CLASS,
    GUIDE_TITLE_CLASS,
    GUIDES_ITEM_CLASS,
    GUIDES_LIST_CLASS,
    GuidesFetcher,
} from "./GuidesFetcher";

const html = `
<div class="${GUIDES_LIST_CLASS}">
    <a href="/url1" class="${GUIDES_ITEM_CLASS}">
        <div class="${GUIDE_TITLE_CLASS}">Guide 1</div>
        <div class="${GUIDE_RATING_CLASS}"><span>Votes:</span>13</div>
    </a>
    <a href="/url2" class="${GUIDES_ITEM_CLASS}">
        <div class="${GUIDE_TITLE_CLASS}">Guide 2</div>
        <div class="${GUIDE_RATING_CLASS}"><span>Votes:</span>14</div>
    </a>
    <a href="/url3" class="${GUIDES_ITEM_CLASS}">
        <div class="${GUIDE_TITLE_CLASS}">Guide 3</div>
        <div class="${GUIDE_RATING_CLASS}"><span>Votes:</span>15</div>
    </a>
    <a class="${GUIDES_ITEM_CLASS}">does not matter</a>
</div>
`;

class MockPageFetcher implements PageFetcher {
    async fetch(url: string): Promise<string> {
        return html;
    }
}

test("simple", async () => {
    const fetcher = new GuidesFetcher(new MockPageFetcher());
    const guides = await fetcher.fetchGuides("whatever", 3);
    expect(guides.length).toBeGreaterThan(0);
    expect(guides.length).toEqual(3);
    const expectedGuides = [0, 1, 2].map((index: number) => {
        return new Guide(
            index,
            `Guide ${index + 1}`,
            13 + index,
            `${MOBAFIRE_DOMAIN}/url${index + 1}`);
    });
    expect(guides).toEqual(expectedGuides);
});
