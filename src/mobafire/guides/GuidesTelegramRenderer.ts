import {Guide} from "./Guide";

const renderGuide = (guide: Guide): string => {
    const lines: string[] = [
        `Guide #${guide.index + 1}`,
        guide.description,
        `Votes: ${guide.votes}`,
        `[Link](${guide.url})`,
    ];
    return lines.join("\n");
};

export class GuidesTelegramRenderer {
    render(guides: Guide[]): string {
        const renderedGuides = guides.map(renderGuide);
        return renderedGuides.join("\n----------\n");
    }
}
