import FuzzySet from "fuzzyset";
import * as _ from "lodash";
import {CHAMPIONS_CONFIG} from "../configs/champions";
import {Champion} from "../configs/champions/Champion";

const fuzzyset = FuzzySet();
CHAMPIONS_CONFIG.forEach((champion: Champion) => {
    fuzzyset.add(champion.text);
});
const championsByText = _.keyBy(CHAMPIONS_CONFIG, "text");

export const findChampions = (query: string): Champion[] => {
    const result = fuzzyset.get(query);
    if (_.isNull(result)) {
        return [];
    }
    return result.map(([score, text]) => {
        return championsByText[text];
    });
};
