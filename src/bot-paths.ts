import * as path from "path";

export const SRC_DIR = path.resolve(__dirname);
export const TOP_DIR = path.dirname(SRC_DIR);
