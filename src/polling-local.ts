import * as fs from "fs";
import * as path from "path";
import {TOP_DIR} from "./bot-paths";
import {setupBot} from "./setup-bot";

const token = fs.readFileSync(path.join(TOP_DIR, "bot-token"), "utf8");
const bot = setupBot(token);
bot.launch();
