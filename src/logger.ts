import * as _ from "lodash";
import * as winston from "winston";

export const logger = winston.createLogger({
    transports: [new winston.transports.Console()],
    format: winston.format.printf((info) => {
        const {message, level, ...kvPairs} = info;
        const kv = _.map(kvPairs, (value: any, key: string) => {
            return `${key}=${JSON.stringify(value)}`;
        }).join(" ");
        return `${level} ${message} ${kv}`;
    }),
});
