import * as fs from "fs";
import * as yaml from "js-yaml";
import * as path from "path";
import {TOP_DIR} from "../../bot-paths";
import {Champion} from "./Champion";

const readChampionsConfig = (): Champion[] => {
    const configPath = path.join(TOP_DIR, "configs/champions.yaml");
    return yaml.safeLoad(fs.readFileSync(configPath, "utf8"));
};

export const CHAMPIONS_CONFIG = readChampionsConfig();
