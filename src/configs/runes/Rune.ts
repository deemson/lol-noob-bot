export class Rune {
    constructor(readonly text: string, readonly mobafireID: string = text) { }
}
