export class Keystone {
    constructor(readonly text: string, readonly mobafireID: string = text) { }
}
