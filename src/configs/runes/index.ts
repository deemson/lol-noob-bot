import * as fs from "fs";
import * as yaml from "js-yaml";
import * as path from "path";
import {TOP_DIR} from "../../bot-paths";
import {Config} from "./Config";
import {Keystone} from "./Keystone";
import {Path} from "./Path";
import {Rune} from "./Rune";
import {Shard} from "./Shard";

const parseRune = (rawRune: any): Rune => {
    return new Rune(rawRune.text, rawRune.mobafireID);
};

const parseKeystone = (rawKeystone: any): Keystone => {
    return new Keystone(rawKeystone.text, rawKeystone.mobafireID);
};

const parsePath = (rawPath: any): Path => {
    return new Path(
        rawPath.text,
        rawPath.keystones.map(parseKeystone),
        rawPath.runes.map((runesSlot: any) => {
            return runesSlot.map(parseRune);
        }),
        rawPath.mobafireID,
    );
};

const parseShard = (rawShard: any): Shard => {
    return new Shard(rawShard.text, rawShard.mobafireID);
};

const readRunesConfig = (): Config => {
    const configPath = path.join(TOP_DIR, "configs/runes.yaml");
    const rawConfig = yaml.safeLoad(fs.readFileSync(configPath, "utf8"));
    return new Config(rawConfig.paths.map(parsePath), rawConfig.shards.map((shardSlot: any) => {
        return shardSlot.map(parseShard);
    }));
};

export const RUNES_CONFIG = readRunesConfig();
