export class Shard {
    constructor(readonly text: string, readonly mobafireID: string = text) { }
}
