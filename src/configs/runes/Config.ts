import * as _ from "lodash";
import {Path} from "./Path";
import {Shard} from "./Shard";

export class Config {
    private pathsByMobafireID: Record<string, Path> = _.keyBy(this.paths, "mobafireID");
    private shardsSlotsByMobafireID: Array<Record<string, Shard>>;

    constructor(readonly paths: Path[],
                readonly shards: Shard[][]) {
        this.shardsSlotsByMobafireID = shards.map((shardSlot) => {
            return _.keyBy(shardSlot, "mobafireID");
        });
    }

    pathByMobafireID(id: string): Path {
        const path = this.pathsByMobafireID[id];
        if (_.isUndefined(path)) {
            throw Error(`path "${id}" does not exist`);
        }
        return path;
    }

    shardsMobafire(id1: string, id2: string, id3: string): [Shard, Shard, Shard] {
        const [shard1, shard2, shard3] = [id1, id2, id3].map((id, idx) => {
            const shard = this.shardsSlotsByMobafireID[idx][id];
            if (_.isUndefined(shard)) {
                throw Error(`shard "${id}" does not exist for slot#${idx + 1}`);
            }
            return shard;
        });
        return [shard1, shard2, shard3];
    }
}
