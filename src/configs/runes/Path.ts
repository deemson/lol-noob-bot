import * as _ from "lodash";
import {Keystone} from "./Keystone";
import {Rune} from "./Rune";

export class Path {
    private keystonesByMobafireID: Record<string, Keystone> = _.keyBy(this.keystones, "mobafireID");
    private runesByMobafireID: Record<string, [number, Rune]> = {};

    constructor(readonly text: string,
                readonly keystones: Keystone[],
                readonly runes: Rune[][],
                readonly mobafireID: string = text) {
        runes.forEach((runesSlot: Rune[], slotNumber: number) => {
            runesSlot.forEach((rune: Rune) => {
                this.runesByMobafireID[rune.mobafireID] = [slotNumber + 1, rune];
            });
        });
    }

    keystoneByMobafireID(id: string): Keystone {
        const keystone = this.keystonesByMobafireID[id];
        if (_.isUndefined(keystone)) {
            throw Error(`keystone "${id}" does not exist for path "${this.text}"`);
        }
        return keystone;
    }

    runesMobafirePrimary(id1: string, id2: string, id3: string): [Rune, Rune, Rune] {
        const [rune1, rune2, rune3] = this.runesMobafire([id1, id2, id3]);
        return [rune1, rune2, rune3];
    }

    runesMobafireSecondary(id1: string, id2: string): [Rune, Rune] {
        const [rune1, rune2] = this.runesMobafire([id1, id2]);
        return [rune1, rune2];
    }

    private runesMobafire(ids: string[]): Rune[] {
        const selectedSlots: boolean[] = [];
        return ids.map((id) => {
            const result = this.runesByMobafireID[id];
            if (_.isUndefined(result)) {
                throw Error(`rune "${id}" does not exist for path "${this.text}"`);
            }
            const [slot, rune] = result;
            if (selectedSlots[slot]) {
                throw new Error(`rune for slot ${slot} is already selected`);
            }
            selectedSlots[slot] = true;
            return rune;
        });
    }
}
