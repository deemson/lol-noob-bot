import {logger} from "../src/logger";

const meta = {a: "a", b: "b", c: "c"};
logger.info("message info", meta);
logger.warn("message warning", meta);
logger.error("message error", meta);
