import axios from "axios";
import * as cheerio from "cheerio";
import * as fs from "fs";
import * as yaml from "js-yaml";
import * as path from "path";
import {TOP_DIR} from "../src/bot-paths";
import {AxiosPageFetcher} from "../src/common/AxiosPageFetcher";
import {MOBAFIRE_DOMAIN} from "../src/mobafire/guides/domain";

const championIDRe = /\/league-of-legends\/champion\/([A-Za-z\-]+)-\d+/;

const writeConfig = async (champions: object[]): Promise<void> => {
    const data = yaml.safeDump(champions);
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(TOP_DIR, "configs/champions.yaml"), data, (err: Error) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

(async () => {
    const pageFetcher = new AxiosPageFetcher(axios);
    const html = await pageFetcher.fetch(`${MOBAFIRE_DOMAIN}/league-of-legends/champions`);
    const $ = cheerio.load(html);
    const champions: object[] = [];
    $(".champ-list .champ-list__item").each((idx: number, el: CheerioElement) => {
        const text = $(".champ-list__item__name b", el).first().text();
        const mobafireID = el.attribs.href.match(championIDRe)[1];
        champions.push({text, mobafireID});
    });
    await writeConfig(champions);
})();
